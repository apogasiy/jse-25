package com.tsc.apogasiy.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

public class AbstractOwnerEntity extends AbstractEntity {

    @Getter
    @Setter
    @NotNull
    protected String userId;

}
