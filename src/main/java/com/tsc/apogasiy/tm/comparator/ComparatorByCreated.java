package com.tsc.apogasiy.tm.comparator;

import com.tsc.apogasiy.tm.api.entity.IHasCreated;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class ComparatorByCreated implements Comparator<IHasCreated> {

    private static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {
    }

    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null)
            return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
