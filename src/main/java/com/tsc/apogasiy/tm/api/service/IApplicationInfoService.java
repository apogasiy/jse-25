package com.tsc.apogasiy.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IApplicationInfoService {

    @NotNull String getApplicationVersion();

    @NotNull String getDeveloperEmail();

    @NotNull String getDeveloperName();

}
