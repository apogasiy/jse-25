package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProjectTasks(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
        @NotNull List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(project.getUserId(), project.getId());
        if (tasks.size() <= 0)
            throw new TaskNotFoundException();
        for (Task task : tasks) {
            System.out.println("Id: " + task.getId());
            System.out.println("Name: " + task.getName());
            System.out.println("Description: " + task.getDescription());
            System.out.println("Status: " + task.getStatus());
        }
    }

    @NotNull
    protected Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return new Project(userId, name, description);
    }

}
